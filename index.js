let express = require('express')
let bodyParser = require('body-parser');
// Import Mongoose
let mongoose = require('mongoose');
// Initialize the app
let app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/wizni');
var db = mongoose.connection;
// Setup server port
var port = process.env.PORT || 8080;

let apiRoutes = require('./routes/routes');
app.use('/api', apiRoutes);

var contactController = require('./controller/controller');

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express and nodemon'));
// Launch app to listen to specified port
app.listen(port, function () {
     console.log("Running Employee Directory on port " + port);
});

