var mongoose = require('mongoose');
// Setup schema
var projectSchema = mongoose.Schema({
    projectId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Contact model
var Project = module.exports = mongoose.model('project', projectSchema);